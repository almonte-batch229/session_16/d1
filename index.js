//console.log("Hello World!")

// Arithemtic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of Addition Operator: " + sum);

let difference = x - y;
console.log("Result of Subtraction Operator: " + difference);

let product = x * y;
console.log("Result of Multiplication Operator: " + product);

let quotient = x / y;
console.log("Result of Division Operator: " + quotient);

// Modulus (%) gets the remainder from 2 divided values
let modulus = y % x;
console.log("Result of Modulo Operator: " + modulus);

// Assignment Operator (=)

let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;
console.log("Result of Addition Assignment Operator: " + assignmentNumber);

// Shoorthand method for assignment operator
assignmentNumber += 2;
console.log("Result of Addition Assignment Operator: " + assignmentNumber);

assignmentNumber -= 2;
console.log("Result of Subtraction Assignment Operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of Multiplication Assignment Operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of Division Assignment Operator: " + assignmentNumber);

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of MDAS Operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of PEMDAS Operation: " + pemdas);

// Incrementation(++) vs Decrementation(--)

let z = 1;

// ++z added 1 to its original value
let increment = ++z;
console.log("Result of pre-incrementation: " + increment);
console.log("Result of pre-incrementation: " + z);

increment = z++;
console.log("Result of post-incrementation: " + increment);
console.log("Result of post-incrementation: " + z);

let decrement = --z;
console.log("Result of pre-decrementation: " + decrement);
console.log("Result of pre-decrementation: " + z);

decrement == z--;
console.log("Result of post-decrementation: " + decrement);
console.log("Result of post-decrementation: " + z);

// Type coercion
let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let noncoercion = numC + numD;
console.log(noncoercion);
console.log(typeof noncoercion);

// false == 0
let numE = false + 1;
console.log(numE);

// true == 1
let numF = true + 1;
console.log(numF);

// Comparison Operators

let juan = "juan";

// Equality Operator (==)
// Checks 2 operands if they are equal/have the same content and may return a boolean value
console.log(1 == 1);
console.log(1 == 2);
console.log(1 == "1"); 
console.log("juan" == "juan"); 
console.log("juan" == juan); 

// Inequality Operator (!=)
// ! == not
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != "1"); 
console.log("juan" != "juan"); 
console.log("juan" != juan);
console.log(0 != false);

// Strict Equality Operator (===)
// compares the content and the data type
console.log(1 === 1); 
console.log(1 === 2);
console.log("juan" === juan); 

// Strict Inequality Operatot (!==)
console.log(1 !== 1); 
console.log(1 !== 2);
console.log("juan" !== juan); 

// Relational Operators
let a = 50;
let b = 65;

// Greater than sign (>)
let isGreaterThan = a > b;
console.log(isGreaterThan);

// Less than sign (<)
let isLessThan = a < b;
console.log(isLessThan);

// Greater than or equal sign (>=)
let isGTorEqual = a >= b;
console.log(isGTorEqual);

// Less than or equal sign (<=)
let isLTorEqual = a <= b;
console.log(isLTorEqual);

// Logical Operators
let isLegalAge = true;
let isRegistered = false;

//Logical AND Operator (&&)
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

// Logical OR Operator (||)
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Logical NOT Operator (!)
let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT operator: " + someRequirementsNotMet);